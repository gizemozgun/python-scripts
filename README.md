# python-scripts

python script that outputs the total number of words in the txt file or how many times the words given as input are repeated 

the following command can be run to output the total word count:

`python index.py run count_words -filename input.txt`

The following command can be run to output the most used "n" words and how many times these words are used:

`python index.py run count_frequency -filename input.txt -n 5 `
