from collections import Counter
import sys
import argparse

def count_words(file_name):
    words  = 0
    with open(file_name) as f:
        data = f.read()
        words = data.split()

    print('Number of words in text file :', len(words))

opts = [opt for opt in sys.argv[1:] if opt.startswith("-")]
args = [arg for arg in sys.argv[1:] if not arg.startswith("-")]

if opts[0] == "-filename":
    file_name = args[0]

count_words(file_name)


def count_frequency(file_name,number):

    with open(file_name) as f:
        wordcount = Counter(f.read().split())

    for k, v in wordcount.most_common(int(number)):
        print(k, v)

opts = [opt for opt in sys.argv[1:] if opt.startswith("-")]
args = [arg for arg in sys.argv[1:] if not arg.startswith("-")]


if opts[0] == "-filename":
    file_name = args[0]
    number = args[1]
else:

    file_name = args[1]
    number = args[0]

#parser = argparse.ArgumentParser()
#parser.add_argument("run", type=str)
#args = parser.parse_args()


#run_functions = {
#        "run count_words": count_words(file_name),
#        "run count_frequency": count_frequency(file_name,number),
        
#        }

count_frequency(file_name,number)
